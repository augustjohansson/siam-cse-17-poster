======================================================

MultiMesh: FEM on Arbitrarily Many Intersecting Meshes

======================================================

This is the repository for the poster on MultiMesh at the SIAM CSE '17 meeting, Atlanta, GA, 2017. 

In the repository you will find the poster as a pdf as well as the papers relevant to the poster. As of Feb 28, 2017, these are

* High order cut finite element methods for the Stokes problem, 
https://link.springer.com/article/10.1186/s40323-015-0043-7

* Simulation of flow and view with applications in computational design of settlement layouts
https://arxiv.org/abs/1610.02277

Please don't hesitate to send me an email if you have any questions.

August Johansson

august@simula.no